package base.service;

import java.util.Random;

import base.cartas.Carta;
import base.cartas.TipoCarta;

public interface CartaService { 
	Carta geraCartaAleatoria(Random generator, int maxMana, int maxAtaque, int maxVida, TipoCarta tc);
	int radInt (Random generator, int min, int max);
}
