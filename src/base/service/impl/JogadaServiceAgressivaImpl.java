package base.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import base.Jogada;
import base.Mesa;
import base.cartas.Carta;
import base.cartas.Lacaio;
import base.cartas.magia.Dano;
import base.service.JogadaService;

public class JogadaServiceAgressivaImpl implements JogadaService {
	
	public int compare (Carta card1, Carta card2){
		Dano mag1 = (Dano) card1;
		Dano mag2 = (Dano) card2;
		if (mag1.getDano() < mag2.getDano()) {
            return -1;
        }
        if (mag1.getDano() > mag2.getDano()) {
            return 1;
        }
        return 0;
	}
	
	class DanoComparator implements Comparator<Carta>{
		
		DanoComparator(){}
		
		public int compare (Carta card1, Carta card2){
			Dano mag1 = (Dano) card1;
			Dano mag2 = (Dano) card2;
			if (mag1.getDano() < mag2.getDano()) {
	            return -1;
	        }
	        if (mag1.getDano() > mag2.getDano()) {
	            return 1;
	        }
	        return 0;
		}
		
	}
	
	class LacaioComparator implements Comparator<Carta>{
		
		LacaioComparator(){}
		
		public int compare (Carta card1, Carta card2){
			Lacaio lac1 = (Lacaio) card1;
			Lacaio lac2 = (Lacaio) card2;
			if (lac1.getAtaque() < lac2.getAtaque()) {
	            return -1;
	        }
	        if (lac1.getAtaque() > lac2.getAtaque()) {
	            return 1;
	        }
	        return 0;
		}
	}
	
	public JogadaServiceAgressivaImpl(){}
	
	@Override
	public List<Jogada> criaJogada(Mesa mesa, char jogador) {
		ArrayList<Carta> lacaios = (jogador == 'P') ? mesa.getLacaiosP() : mesa.getLacaiosS();
		Collections.sort(lacaios, new LacaioComparator());
		List<Jogada> minhasJogadas = new ArrayList<>();
		List<Carta> danosArea = new ArrayList<>();
		ArrayList<Carta> mao = (jogador == 'P') ? mesa.getMaoP() : mesa.getMaoS();

		danosArea.addAll(mao.stream()
				.filter(carta -> carta instanceof Dano)
				.sorted(new DanoComparator())
				.collect(Collectors.toList()));
		
		//realiza a invocação de carta Dano
		for (Carta dano : danosArea) {
			if (mesa.getMana(jogador) >= dano.getCustoMana()) {
				minhasJogadas.add(new Jogada(dano, null, jogador));
				mesa.sacarCarta(jogador);
				mesa.decMana(dano.getCustoMana(), jogador);
			}
			else{
				break;
			}
		}
		
		//invoca os lacaios com alvo no herói inimigo
		for (Carta carta : lacaios) {
			minhasJogadas.add(new Jogada(carta, null, jogador));
		}
		
		return minhasJogadas;
	}
}
