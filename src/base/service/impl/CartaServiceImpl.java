package base.service.impl;

import java.util.List;
import java.util.Random;

import base.Jogada;
import base.Mesa;
import base.cartas.Carta;
import base.cartas.HabilidadesLacaio;
import base.cartas.Lacaio;
import base.cartas.TipoCarta;
import base.cartas.magia.Buff;
import base.cartas.magia.Dano;
import base.cartas.magia.DanoArea;
import base.service.CartaService;
import util.*;

public class CartaServiceImpl implements CartaService{
	private RandomString stringGenerator;
	private HabilidadesLacaio habilidade;
	private TipoCarta escolhido;
	
	public CartaServiceImpl(){}
	
	@Override
	public Carta geraCartaAleatoria(Random generator, int maxMana, int maxAtaque, int maxVida, TipoCarta tc){
		Carta aux = null;
		RandomString stringGerador = new RandomString(generator, Util.MAX_NOME);
		
		if (tc == TipoCarta.BUFF){
			aux = new Buff(stringGerador.nextString(), radInt(generator,1,maxMana), radInt(generator,1,maxAtaque), radInt(generator,1,maxVida));
		}
		else if (tc == TipoCarta.DANO){
			aux = new Dano(stringGerador.nextString(), radInt(generator,1,maxMana), radInt(generator,1,maxAtaque));
		}
		else if (tc == TipoCarta.DANO_AREA){
			aux = new DanoArea(stringGerador.nextString(), radInt(generator,1,maxMana), radInt(generator,1,maxAtaque));
		}
		else if (tc == TipoCarta.LACAIO){
			aux = new Lacaio(stringGerador.nextString(), radInt(generator,1,maxMana), radInt(generator,1,maxAtaque), radInt(generator,1,maxVida), maxVida,
					escolheHabilidade(generator));
		}
		else{
			tc = escolheTipo (generator);
			return geraCartaAleatoria(generator, maxMana, maxAtaque,  maxVida, tc);
		}
		
		return aux;
	}

	@Override
	public int radInt(Random generator, int min, int max) {
		return generator.nextInt((max - min) + 1) + min;
	}
	
	public static HabilidadesLacaio escolheHabilidade (Random generator){
		int escolha = radIntAux(generator,1,3);
		
		if(escolha == 1)
			return HabilidadesLacaio.EXAUSTAO;
		else if (escolha == 2)
			return HabilidadesLacaio.INVESTIDA;
		else
			return HabilidadesLacaio.PROVOCAR;
	}
	
	public static TipoCarta escolheTipo (Random generator){
		int escolha = radIntAux(generator,1,4);
		
		if(escolha == 1)
			return TipoCarta.BUFF;
		else if (escolha == 2)
			return TipoCarta.DANO;
		else if (escolha == 3)
			return TipoCarta.DANO_AREA;
		else 
			return TipoCarta.LACAIO;
	}
	
	public static int radIntAux (Random generator, int min, int max){
		return generator.nextInt((max - min) + 1) + min;
	}

	public HabilidadesLacaio getHabilidade() {
		return habilidade;
	}

	public void setHabilidade(HabilidadesLacaio habilidade) {
		this.habilidade = habilidade;
	}

	public TipoCarta getEscolhido() {
		return escolhido;
	}

	public void setEscolhido(TipoCarta escolhido) {
		this.escolhido = escolhido;
	}

	public RandomString getStringGenerator() {
		return stringGenerator;
	}

	public void setStringGenerator(RandomString stringGenerator) {
		this.stringGenerator = stringGenerator;
	}
}
