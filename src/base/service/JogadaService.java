package base.service;

import java.util.List;

import base.Mesa;
import base.Jogada;

public interface JogadaService {
	List<Jogada> criaJogada(Mesa mesa, char jogador);
}