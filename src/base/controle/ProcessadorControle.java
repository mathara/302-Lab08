package base.controle;

import base.Jogada;
import base.Mesa;
import base.service.ProcessadorService;
import base.service.impl.ProcessadorServiceImpl;

public class ProcessadorControle {
	ProcessadorService processadorControle ;
	
	public ProcessadorControle(ProcessadorService processador){
		this.processadorControle = processador;
	}
	
	public ProcessadorControle(){
		this.processadorControle = new ProcessadorServiceImpl();
	}
	
	boolean processar(Jogada jogada, Mesa mesa){
		return this.processadorControle.processar(jogada, mesa);
	}
}
